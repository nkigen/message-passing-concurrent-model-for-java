package org.fairsem;

import java.util.LinkedList;
import java.util.List;

public class fairSemaphore {
	List<Thread> waitList;
	int value;

	public fairSemaphore(int value) {
		this.value = value;
		waitList = new LinkedList<Thread>();
	}

	public synchronized void P() {
		waitList.add(Thread.currentThread());

		while (value == 0 || !Thread.currentThread().equals(waitList.get(0))) {
			try {
				wait();
			} catch (InterruptedException e) {
			}
		}

		waitList.remove(Thread.currentThread());
		value--;

		if (value > 0 && !waitList.isEmpty())
			notifyAll();

	}

	public synchronized void V() {
		value++;
		notifyAll();

	}

}
