package org.port;

/*
 * 
 -public <T> void send(T message): Blocks the calling thread until the receive() operation is called. Thereafter the message is added to the port buffer and the 			
 thread waiting on the receive() operation is awakened.
 -public <T> T receive(): A thread calling receive() signals that its ready to receive by calling the V() operation on semaphore sem_continue before 
 blocking on sem_avail waiting for a thread to call send() or insert a message in the buffer.

 */
import java.util.LinkedList;
import java.util.List;

import org.fairsem.fairSemaphore;

public class Port<T> {

	private fairSemaphore sem_avail;
	private fairSemaphore sem_continue;
	//private fairSemaphore mutex;
	private Object buffer;
	private boolean free;

	public Port() {
		sem_avail = new fairSemaphore(0);
		//mutex = new fairSemaphore(1);
		sem_continue = new fairSemaphore(0);
		free=true;
		// buffer = new Object();
	}

	public <T> void send(T message) {
		sem_continue.P();
		//mutex.P();
		buffer = message;
		free=false;
		//mutex.V();
		sem_avail.V();
	}

	public <T> T receive() {

		sem_continue.V();
		sem_avail.P();
		//mutex.P();
		T message = (T) buffer;
		free=true;
		//mutex.V();
		return message;
	}
	
	public boolean isEmpty()
	{
		return free;
	}

}
