package org.port;

public class PortNotAvailable extends Exception {

	
	public PortNotAvailable()
	{
		super("The port index is not available!!");
	}
}
