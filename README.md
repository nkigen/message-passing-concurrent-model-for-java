package org.fairsem

1) fairSemaphore.java
Implements class fairSemaphore which has two methods: P() and V().
V() increments the semaphore value and P() decrements the value and blocks the thread if the value=0 or its not the first thread in the waitList.
The semaphore value is initialized by passing the initialization value to the constructer.
	fairSemaphore fair=new fairSemaphore(value);

waitList contains references to threads currently blocked on the semaphore. 
The waiting threads are awakened in a FIFO order by removing the thread at the head of the list.


***********************************************************************************************************************************************************
package org.port

1)Port.java
Implements class Port which simulates a generic synchronous ports.
It contains 2 methods:
-public <T> void send(T message): Blocks the calling thread until the receive() operation is called. Thereafter the message is added to the port buffer and the 							  thread waiting on the receive() operation is awakened.
-public <T> T receive(): A thread calling receive() signals that its ready to receive by calling the V() operation on semaphore sem_continue before 
						 blocking on sem_avail waiting for any thread to insert a message on the buffer.
-public void isEmpty(): returns true if the port has no pending message and false otherwise.

Semaphore mutex is used by both the send() and receive() operations for mutual exclusion when accessing the buffer.

***********************************************************************************************************************************************************
package org.port

1)portNotAvailable.java
Implements class portNotAvailable which extends Exception class and is thrown when request() operation is called which an invalid port index.

2)Ports.java
 Implements class Ports which simulates a set of identical Ports as implemented in class org.ports.Port.
 The number of ports in the array is set when creating the object.
 The following semaphores are used:
	-sem_send_mutex- Guarantees mutual exclusion for threads calling the send() operation.
	-mutex- Used for mutual exclusion when accessing the variables shared between the send() and receive() operations.
	-sem_receive-used to block the receive operation if no thread is ready to send a message on any of the ports in pa. Its value is initially 0.
 Contains two public methods and 1 private method.
 
	public methods
 -	public <T> void send(T message, int p) throws PortNotAvailable - Sends a message to port at index p and it throws PortNotAvailable exception if index p
																	 is invalid.
																	 Otherwise a counter of the threads ready to send a message on the specified port is incremented and sem_receive is signalled hence the receive operation can continue.
																   
 - public <T> T receive():- Reads a message from any of the ports in pa that have messages.
							When called, it blocks on sem_receive waiting for any thread to call the send operation.
							This operation blocks if no thread has called the send operation.ie. If there is no message in any port.
							If a message is available from multiple ports, one of the ports is selected at random by the method selectPort() and a message is read from it.
	
	private method(s)
-private int selectPort(): -Selects an index at random from the non empty ports in pa( ports array)

***********************************************************************************************************************************************************
package org.server

1) Server.java
Implements class Server which a server used to allocate a set of 5 identical resources.
The server has an array of 3 Ports called comm_ports: comm_ports[0] receives requests from clients belonging to setA
													  comm_ports[1] receives requests from clients belonging to setA
													  comm_ports[2] receives requests from clients belonging to both setA and setB wishing to release a resource
When requesting a resource, a client sends data of type Message which contains a reference to the port to be used for the reply and the data to be send.
For request messages, the data part of the message is null while for release messages the data part contains an integer of the resource to be released.

The server checks for messages from each port by calling the isEmpty function of the Port class which returns true if a message has been send to the port and is waiting to be received. If a message if available, method receiveRequest() returns an integer corresponding to the index in comm_ports array which has a message. The messge is then read from the port. If the message is a request message from either setA or setB and a free resource is available, one is allocated, the reply port is read from the message and a reply is send back to the client. If no resource is free, the client is blocked and the request is queued either to waiting_setA or waiting_setB for setA and setB requests respectively.
On the other hand if the received message was a release message, a reply signal is send back to the client releasing the resource then if setA has blocked messages, one of then is picked and the resource is allocated to it and a reply is send. if no setA clients have been but there are setB clients blocked, the resource if allocated to one of them and its removed from the queue. Finally, if no client has been blocked, the resource is marked as free.

				







